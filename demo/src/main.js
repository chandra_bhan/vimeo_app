import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vueVimeoPlayer from 'vue-vimeo-player'

Vue.use(vueVimeoPlayer)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

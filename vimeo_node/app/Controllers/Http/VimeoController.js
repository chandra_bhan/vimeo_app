'use strict'
const Helpers = use('Helpers');
const fs = require('fs');
let Vimeo = require('vimeo').Vimeo;
// let client = new Vimeo("a350bcaa40cc93ad1721b247ca10435eb4a6ce59", "dQjeV0B34mRCeejVigD2fhHfGac65zbw3d1/D8eDH9x8ranNLHn9rrrcSce3LIwj06IhRdFR+01N4N2ai4nibAw0TWlb4oOIXhYZetV+9FC3AxOXpM+iVyDVqoJ45yNm", "eeda6201760add1ea5c7fe8b60c0556c");
let client = new Vimeo("0ee01774807923bee78599f62a514b5f9c9b4f56", "WAnsjTs6EEHY4TNBLh6s5IIVzEDTDtkRI3i1nb8NNCXwmdaYKjWYA5at3MNFRNl2nopVHWO2yKshFW6lsB1DPETX4f70VGC34nTHCALlc9zzs1G7AbIKHCqESXo/89NY", "b928feeff3db7f6e9b9bf7d83fea3c4c");


class VimeoController {
    
  // Upload Video
    async uploadVideo({request, response}){
      
      const file_title = request.input('file_title')
      const file_desc = request.input('file_desc')
       fs.unlink("./public/uploads/video", (err) => {
        if (err)  console.log(err);
        else console.log('video removed from public');                                 
      });
        let file_name = request.file('file_name')
        await file_name.move(Helpers.publicPath('uploads'), { name: 'video'})
        file_name = '/var/www/html/Vimeo app/vimeo_node/public/uploads/video'
        client.upload(
          file_name,
          {
            'name': file_title,
            'description': file_desc
          },
          function (uri) {
            console.log('Your video URI is: ' + uri);
          },
          function (bytes_uploaded, bytes_total) {
            var percentage = (bytes_uploaded / bytes_total * 100).toFixed(2)
            console.log(bytes_uploaded, bytes_total, percentage + '%')
          },
          function (error) {
            console.log('Failed because: ' + error)
          }
        )
        return "Video Uploded Successfully";
    }

    // All Videos
    async allVideos({request, response}){
      let uri ="/me/videos"
      await client.request({
                path: uri,
                query: {
                  fields: 'uri,name,description,duration,created_time,modified_time'
                }
              }, function (error, body) {
                if (error) {
                  console.log('error');
                  console.log(error);
                } else {
                  console.log('body');  
                  const mydata = body.data
                  console.log(mydata); 
                 
                }
              })
              return response.send( "hayyy " + mydata);
            }

    // Edit Video 
    async editVideo({request,response}){
       let uri ="/videos/402487431"
            client.request({
                method: 'PATCH',
                path: uri,
                query: {
                  'name': 'new video title',
                  'description': 'hayyyyyyyyyyyy'
                }
              }, function (error, body, status_code, headers) {
                  if(error){
                      console.log(error)
                  }
                
                  if(status_code){
                      console.log("Status Code :          " + status_code)
                  }
                
                console.log('The title and description for ' + uri + ' has been edited.')
              })     
      }
}
module.exports = VimeoController
